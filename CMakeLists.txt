cmake_minimum_required(VERSION 3.19)
project(googletest)

set(OUR_DIR ${CMAKE_CURRENT_SOURCE_DIR})

set(TARGET                  googletest)
set(TEST_SRC_DIR            ${OUR_DIR}/googletest/src)
set(TEST_PUBLIC_HDR_DIR     ${OUR_DIR}/googletest/include)
set(TEST_PRIVATE_HDR_DIR    ${OUR_DIR}/googletest)

set(MOCK_SRC_DIR            ${OUR_DIR}/googlemock/src)
set(MOCK_PUBLIC_HDR_DIR     ${OUR_DIR}/googlemock/include)
set(MOCK_PRIVATE_HDR_DIR    ${OUR_DIR}/googlemock)

set(SRCS
    ${TEST_SRC_DIR}/gtest-all.cc
    ${MOCK_SRC_DIR}/gmock-all.cc
)

if (MSVC)
  add_definitions(/Zc:__cplusplus)
endif()

include_directories(
    ${TEST_PUBLIC_HDR_DIR}
    ${TEST_PRIVATE_HDR_DIR}
    ${MOCK_PUBLIC_HDR_DIR}
    ${MOCK_PRIVATE_HDR_DIR}
)

add_library(${TARGET} STATIC ${SRCS})

if (LINUX)
    set(THREADS_PREFER_PTHREAD_FLAG ON)
    find_package(Threads REQUIRED)
    target_link_libraries(${TARGET} Threads::Threads)
endif()

target_include_directories(${TARGET} 
PUBLIC 
    ${TEST_PUBLIC_HDR_DIR}
    ${MOCK_PUBLIC_HDR_DIR}
)

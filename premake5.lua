project "GoogleTest"
    kind "StaticLib"
    language "C++"
    cppdialect "C++20"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-obj/" .. outputdir .. "/%{prj.name}")

    files { 
        "googlemock/src/**.cc", 
        "googlemock/include/**.h", 

        "googletest/src/**.cc", 
        "googletest/src/**.h", 
        "googletest/include/**.h", 
    }

    includedirs {
        "googlemock",
        "googlemock/include",
        "googletest",
        "googletest/include"
    }

    filter "system:windows"
    systemversion "latest"

    filter "configurations:Debug"
        symbols "On"

    filter "configurations:Release"
        optimize "On"
